const axios = require('axios');

const fetchApi = async () => {
  let urlUpdateProduct = 'https://apiforseller.docs.apiary.io/#reference/product/update-product';
  let urlUpdatePrice = 'https://apiforseller.docs.apiary.io/#reference/product/update-price';
  let urlUpdateStock = 'https://apiforseller.docs.apiary.io/#reference/product/update-stock';
  let data = {};

  try {
    // const responsePosts = await axios.get(urlPosts); // Wait to finish
    // const responseUsers = await axios.get(urlUsers); // Wait to finish
    // const responseAlbums = await axios.get(urlAlbums); // Wait to finish
    const response = await Promise.all([
      axios.get(urlUpdateProduct),
      axios.get(urlUpdatePrice),
      axios.get(urlUpdateStock),
    ]);

    // data = {
    //   posts: responsePosts.data.filter((item) => item.userId === 5),
    //   users: responseUsers.data,
    //   albums: responseAlbums.data,
    // };

    data = {
      product: response[0].data.filter((item) => item.productId === 5),
      price: response[1].data,
      stock: response[2].data,
    };

    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

fetchApi();
