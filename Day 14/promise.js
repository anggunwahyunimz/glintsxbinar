const axios = require('axios');
const { promises } = require('stream');

let urlUpdateProduct = 'https://apiforseller.docs.apiary.io/#reference/product/update-product';
let urlUpdatePrice = 'https://apiforseller.docs.apiary.io/#reference/product/update-price';
let urlUpdateStock = 'https://apiforseller.docs.apiary.io/#reference/product/update-stock';
let data = {};



axios
  .get(urlUpdateProduct)
  .then((response) => {
    data = { product: response.data.filter((item) => item.productId === 5) };

    return axios.get(urlUpdatePrice);
  })
  .then((response) => {
    data = { ...data, price: response.data };

    return axios.get(urlUpdateStock);
  })
  .then((response) => {
    data = { ...data, stock: response.data };

    console.log(data);
  })
  .catch((err) => {
    console.log(err.message);
  });
