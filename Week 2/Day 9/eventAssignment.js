const EventEmitter = require("events");
const readline = require("readline"); 


const my = new EventEmitter();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

my.on("login:failed", function (email) {
  console.log(`${email} is failed to login!`);
  rl.close();
});

my.on("login:success", function (email) {
  const patientName = require("../Day 9/dataStructureAssignment");
  if ("login:success") {
    console.log(patientName);
    rl.close();
  }
});

// Function to login
function login(email, password) {
  const passwordStoredInDatabase = "123456";

  if (password !== passwordStoredInDatabase) {
    my.emit("login:failed", email); // Pass the email to the listener
  } else {
    // Do something
    my.emit("login:success", email);
  }
}

// Input email and password
rl.question("Email: ", function (email) {
  rl.question("Password: ", function (password) {
    login(email, password); // Run login function
  });
});

module.exports = rl; // export rl to make another can run the readline
